#include "image.h"

#include <stdlib.h>

bool create_image(struct image *addr, int width, int height){
    addr -> width = width;
    addr -> height = height;

    addr -> data = malloc(width * height * PIXEL_SIZE);
    if(addr -> data == NULL)
        return false;
    return true;
}


void delete_image(struct image *img){
    free(img -> data);
}

bool create_rotated_image(struct image src, struct image *dst){
    if(!create_image(dst, src.height, src.width))
        return false;

    for(int i=0; i<dst->height; i++){
        for(int j=0; j<dst->width; j++){
            uint32_t src_pos = (src.height - j - 1) * src.width + i;
            uint32_t dst_pos = i * dst->width + j;
            dst->data[dst_pos] = src.data[src_pos];
        }
    }

    return true;
}
