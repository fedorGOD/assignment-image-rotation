#ifndef _FORMATS_H_
#define _FORMATS_H_

#include "image.h"
#include <stdint.h>
#include <stdio.h>

struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
}__attribute__((packed));


enum read_status  {
    READ_OK = 0,
    READ_WRONG_BIT_COUNT,
    READ_INVALID_SIGNATURE,
    READ_CANNOT_ALLOC,
    READ_CANNOT_READ
};


enum read_status read_bmp(FILE *f, struct image *addr);
bool write_bmp(FILE *f, struct image image);


#endif
