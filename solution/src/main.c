#include "formats.h"
#include <stdio.h>
#include <stdlib.h>

FILE* try_open(const char* path, const char* mode){
    FILE *f = fopen(path, mode);
    if(!f){
        fprintf(stderr, "Cannot access %s\n", path);
        exit(1);
    }
    return f;
}

int main( int argc, char** argv ) {
    
    if(argc < 3){
        fprintf(stderr, "Usage: %s source.bmp target.bmp\n", argv[0]);
        return 1;
    }

    FILE *in = try_open(argv[1], "r");
    FILE *out = try_open(argv[2], "w");

    struct image img;
    enum read_status status = read_bmp(in, &img);
    if(status != READ_OK){
        if(status == READ_CANNOT_ALLOC)      fprintf(stderr, "Cannot allocate memory\n");
        if(status == READ_INVALID_SIGNATURE) fprintf(stderr, "File %s is not a BMP\n", argv[1]);
        if(status == READ_WRONG_BIT_COUNT)   fprintf(stderr, "Only 24-bit BMPs are supported\n");
        if(status == READ_CANNOT_READ)       fprintf(stderr, "Cannot read %s\n", argv[1]);
        
        delete_image(&img);
        fclose(in);
        fclose(out);
        return 1;
    }

    fclose(in);

    struct image new;
    if(!create_rotated_image(img, &new)){
        fprintf(stderr, "Cannot rotate image\n");
        
        delete_image(&img);
        delete_image(&new);
        fclose(out);
        return 1;
    }

    delete_image(&img);

    if(!write_bmp(out, new)){
        fprintf(stderr, "Cannot write to %s\n", argv[2]);
        delete_image(&new);
        fclose(out);
        return 1;
    }

    delete_image(&new);
    fclose(out);

    return 0;
}
